#ifndef GVWRAPPER_H
#define GVWRAPPER_H

#include <gvc.h>
#include <QString>

namespace ag {
    /// The agopen method for opening a graph
    static inline Agraph_t* agopen(QString name, Agdesc_t kind)
    {
        return ::agopen(const_cast<char *>(qPrintable(name)), kind, 0);
    }

    /// Add an alternative value parameter to the method for getting an object's attribute
    static inline QString agget(void *object, QString attr, QString alt=QString())
    {
        QString str = ::agget(object, const_cast<char *>(qPrintable(attr)));

        if(str==QString())
            return alt;
        else
            return str;
    }

    /// Directly use agsafeset which always works, contrarily to agset
    static inline int agset(void *object, QString attr, QString value)
    {
        return ::agsafeset(object, const_cast<char *>(qPrintable(attr)),
                         const_cast<char *>(qPrintable(value)),
                         const_cast<char *>(qPrintable(value)));
    }

    /// Sets node attributes for the whole graph
    static inline int agnodeattr(Agraph_t *g, QString attr, QString value)
    {
        return ::agsafeset(g, const_cast<char *>(qPrintable(attr)),
                         const_cast<char *>(qPrintable(value)),
                         const_cast<char *>(qPrintable(value)));
    }

    static inline int agnodeattr(Agnode_t *n, QString attr, QString value)
    {
        return ::agsafeset(n, const_cast<char *>(qPrintable(attr)),
                         const_cast<char *>(qPrintable(value)),
                         const_cast<char *>(qPrintable(value)));
    }

    static inline int agedgeattr(Agraph_t *g, QString attr, QString value)
    {
        return ::agsafeset(g, const_cast<char *>(qPrintable(attr)),
                         const_cast<char *>(qPrintable(value)),
                         const_cast<char *>(qPrintable(value)));
    }

    static inline int agedgeattr(Agedge_t *e, QString attr, QString value)
    {
        return ::agsafeset(e, const_cast<char *>(qPrintable(attr)),
                         const_cast<char *>(qPrintable(value)),
                         const_cast<char *>(qPrintable(value)));
    }

    static inline Agnode_t* agnode(Agraph_t *g, QString name)
    {
        return ::agnode(g, const_cast<char *>(qPrintable(name)), 1);
    }

    static inline int gvLayout(GVC_t *gvc, Agraph_t *g, QString engine)
    {
        return ::gvLayout(gvc, g, const_cast<char *>(qPrintable(engine)));
    }

    static inline int agdelete(Agraph_t *g, Agnode_t *n)
    {
        return ::agdelete(g, n);
    }

    static inline int agdelete(Agraph_t *g, Agedge_t *e)
    {
        return ::agdelete(g, e);
    }

    static inline Agnodeinfo_t* data(Agnode_t *n)
    {
        return (Agnodeinfo_t*)AGDATA(n);
    }

    static inline Agedgeinfo_t* data(Agedge_t *e)
    {
        return (Agedgeinfo_t*)AGDATA(e);
    }

    static inline Agraphinfo_t* data(Agraph_t* g)
    {
        return (Agraphinfo_t*)AGDATA(g);
    }
}

#endif // GVWRAPPER_H
