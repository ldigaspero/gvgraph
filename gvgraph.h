#ifndef GVGRAPH_H
#define GVGRAPH_H

#include <QString>
#include <QStringList>
#include <QMap>
#include <QPair>
#include <QFont>
#include <QRectF>
#include <QPainterPath>
#include <QGraphicsScene>
#include <QGraphicsItemGroup>
#include <QApplication>
#include <QDesktopWidget>
#include <cmath>
#include <QDebug>
#include "gvwrapper.h"

enum Shape {
    Ellipse,
    Box
};

template <class Node, class Edge>
class GVGraph;


class AbstractGVGraph : public QGraphicsScene
{
public:
    inline operator Agraph_t* () { return _graph; }
    inline Shape defaultShape() const { return _default_shape; }
    /// Default DPI value used by dot (which uses points instead of pixels for coordinates)
    static const qreal DotDefaultDPI;
    QPointF graphviz2viewport(QPointF p) const;
    QPointF viewport2graphviz(QPointF p) const;
    virtual Agraph_t* gvgraph() const
    { return _graph; }
protected:
    AbstractGVGraph(QString name, Shape shape);
    virtual ~AbstractGVGraph();
    QString _name;
    GVC_t *_context;
    Agraph_t *_graph;
    Shape _default_shape;
};

inline qreal inches2points(qreal in) { return in * AbstractGVGraph::DotDefaultDPI; }
inline qreal points2inches(qreal pt) { return pt / AbstractGVGraph::DotDefaultDPI; }



/// A struct containing the information for a GVGraph's edge
template <class Node>
class GVEdge : public QGraphicsItemGroup
{
    friend class GVGraph<Node, GVEdge<Node> >;
public:
    GVEdge(AbstractGVGraph *g, Node *n1, Node *n2);
    virtual ~GVEdge();
    inline operator Agedge_t* () { return _edge; }
    inline const Node* source() const { return _source; }
    inline const Node* target() const { return _target; }
    inline Node* source() { return _source; }
    inline Node* target() { return _target; }
    virtual void render();
protected:
    /// The reference graph
    AbstractGVGraph* _graph;
    /// The managed edge in the graphviz graph
    Agedge_t* _edge;
    /// The source and target nodes of the edge
    Node* _source;
    Node* _target;

    /// Path of the edge's line
    QPainterPath _path;
    bool _laid_out;
};

template <class Node>
GVEdge<Node>::GVEdge(AbstractGVGraph *g, Node *n1, Node *n2)
    : _graph(g), _source(n1), _target(n2), _laid_out(false)
{
    _edge = ::agedge(*_graph, *_source, *_target, 0, 1);
    _graph->addItem(this);
}

template <class Node>
GVEdge<Node>::~GVEdge()
{
    ag::agdelete(_graph->gvgraph(), _edge);
    _graph->removeItem(this);
}

template <class Node>
void GVEdge<Node>::render()
{
    if (!_laid_out)
    {
        qDebug() << "Trying to render a non laid-out edge: " << _source->name() << ", " << _target->name();
        return;
    }
    addToGroup(new QGraphicsPathItem(_path));
    QPolygonF arrow_f;
    arrow_f << QPointF(0, 0) << QPointF(-0.5, 1) << QPointF(0.5, 1);
    QPointF offset(_path.pointAtPercent(1.0));
    QMatrix matrix;
    matrix.scale(6.0, 6.0); // this is the size for the arrow in points
    matrix.rotate(90.0 - _path.angleAtPercent(1.0));
    arrow_f = matrix.map(arrow_f);
    arrow_f.translate(offset);
    QGraphicsPolygonItem* arrow = new QGraphicsPolygonItem(arrow_f, 0);
    arrow->setBrush(QBrush("#ff0000"));
    addToGroup(arrow);
}

/// An object containing a libgraph graph and its associated nodes and edges
template <class Node, class Edge = GVEdge<Node> >
class GVGraph : public AbstractGVGraph
{
public:
    /*!
     * \brief Construct a Graphviz graph object
     * \param name The name of the graph, must be unique in the application
     * \param font The font to use for the graph
     * \param node_size The size in pixels of each node
     */
    GVGraph(QString name, Shape shape = Box, QFont font=QFont());
    ~GVGraph();

    /// Clears the graph
    void clear();

    /// Add and remove nodes
    Node* addNode(const QString& name);
    Node* addNode(Node* n);
    Node* addNode(const QString& name, qreal width, qreal height);
    QList<Node*> addNodes(const QStringList& names);    
    void removeNode(const QString& name);
    void removeNode(Node* n);
    void clearNodes();

    /// Add and remove edges
    Edge* addEdge(const QString& source, const QString& target);
    Edge* addEdge(Node* n1, Node* n2);
    void removeEdge(const QString& source, const QString& target);
    void removeEdge(Edge* e);
    void removeEdge(Node* n1, Node* n2);
    void clearEdges();

    /// Set the font to use in all the labels
    void setFont(QFont font);

    /// Set the root node, to be used in laying out
    void setRootNode(const QString& name);

    /// Apply the layout to the given graph using the engine passed as parameter
    void applyLayout(QString engine = "dot");

    /// Apply a circular layout to the given graph using the radius passed as parameter
    void circleLayout(qreal radius = 1.0);

    /// Updates the layout after a graph modification
    void updateLayout(bool all = false);

    /// Draws nodes and edges on the graph QGraphicsScene
    virtual void render();

    virtual Node* node(QString name) const
    { return _nodes.contains(name) ? _nodes[name] : 0; }
    virtual Edge* edge(QString n1, QString n2) const
    { return _edges.contains(QPair<QString, QString>(n1, n2)) ? _edges[QPair<QString, QString>(n1, n2)] : 0; }

    virtual QList<Node*> nodes() const;
    virtual QList<Edge*> edges() const;
protected:
    void removeEdge(const QPair<QString, QString>& v);
    void updateNodesLayout(bool all_nodes = false);
    void updateEdgesLayout(bool all_edges = false, QPointF disp = QPointF(0.0, 0.0));
private:
    QFont _font;
    QMap<QString, Node*> _nodes;
    QMap<QPair<QString, QString>, Edge*> _edges;
    bool _laid_out;
};



inline QPointF AbstractGVGraph::graphviz2viewport(QPointF p) const { return QPointF(p.x(), ag::data(_graph)->bb.UR.y - p.y()); }
inline QPointF AbstractGVGraph::viewport2graphviz(QPointF p) const { return QPointF(p.x(), ag::data(_graph)->bb.UR.y - p.y()); }

template <class Node, class Edge>
GVGraph<Node, Edge>::GVGraph(QString name, Shape shape, QFont font) :
    AbstractGVGraph(name, shape), _laid_out(false)
{
    //Set default attributes for the future nodes
    ag::agnodeattr(_graph, "fixedsize", "true");
    ag::agnodeattr(_graph, "label", "");
    ag::agnodeattr(_graph, "regular", "true");

    setFont(font);
}

template <class Node, class Edge>
GVGraph<Node, Edge>::~GVGraph()
{
    clearNodes();
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::clear()
{
    clearNodes();
    agclose(_graph);
    _graph = ag::agopen(_name, Agstrictdirected);
    QGraphicsScene::clear();
}

template <class Node, class Edge>
 Node* GVGraph<Node, Edge>::addNode(const QString& name, qreal width, qreal height)
{
    Node* node = addNode(name);
    node->setWidth(width);
    node->setHeight(height);
    _nodes.insert(name, node);

    return node;
}

template <class Node, class Edge>
Node* GVGraph<Node, Edge>::addNode(const QString& name)
{

    if (_nodes.contains(name))
        removeNode(name);

    Node* node = new Node(this, name);
    switch (_default_shape)
    {
       case Ellipse: ag::agnodeattr(*node, "shape", "ellipse");
         break;
       case Box: ag::agnodeattr(*node, "shape", "box");
         break;
       default: break; // FIXME: an exception should be thrown
    }
    _nodes.insert(name, node);

    return node;
}

template <class Node, class Edge>
Node* GVGraph<Node, Edge>::addNode(Node* node)
{
    // FIXME: the node in principle could belong to a different graph
    if (_nodes.contains(node->name()))
    {
        if (_nodes[node->name()] == node)
            return node;
        else
            _nodes.remove(node->name());
    }

    switch (_default_shape)
    {
       case Ellipse: ag::agnodeattr(*node, "shape", "ellipse");
         break;
       case Box: ag::agnodeattr(*node, "shape", "box");
         break;
       default: break; // FIXME: an exception should be thrown
    }
    _nodes.insert(node->name(), node);

    return node;
}

template <class Node, class Edge>
QList<Node*> GVGraph<Node, Edge>::addNodes(const QStringList& names)
{
    QList<Node*> list;

    for (int i = 0; i < names.size(); i++)
        list << addNode(names.at(i));

    return list;
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::removeNode(const QString& name)
{
    if (_nodes.contains(name))
    {
        Node *node = _nodes[name];
        _nodes.remove(name);

        QList<QPair<QString, QString> > keys= _edges.keys();
        for (int i = 0; i < keys.size(); i++)
            if (keys.at(i).first == name || keys.at(i).second == name)
                removeEdge(keys.at(i));

        delete node;
    }
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::removeNode(Node* node)
{
    if (_nodes.contains(node->name))
    {
        _nodes.remove(node->name);

        QList<QPair<QString, QString> > keys= _edges.keys();
        for (int i = 0; i < keys.size(); i++)
            if (keys.at(i).first == node->name || keys.at(i).second == node->name)
                removeEdge(keys.at(i));

        delete node;
    }
}


template <class Node, class Edge>
void GVGraph<Node, Edge>::clearNodes()
{
    QList<QString> keys = _nodes.keys();

    for (int i = 0; i < keys.size(); i++)
        removeNode(keys.at(i));
    _laid_out = false;
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::clearEdges()
{
    QList<QPair<QString, QString> > keys = _edges.keys();

    for (int i = 0; i < keys.size(); i++)
        removeEdge(keys.at(i));
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::setRootNode(const QString& name)
{
    if (_nodes.contains(name))
        ag::agset(_graph, "root", name);
}


template <class Node, class Edge>
Edge* GVGraph<Node, Edge>::addEdge(const QString &source, const QString &target)
{
    if (_nodes.contains(source) && _nodes.contains(target))
    {
        QPair<QString, QString> key(source, target);
        if (!_edges.contains(key))
        {
            Node *n1 = const_cast<Node*>(_nodes[source]), *n2 = const_cast<Node*>(_nodes[target]);
            Edge *e = new Edge(this, n1, n2);
            _edges.insert(key, e);
            return e;
        }
        else
            return _edges[key];
    }
    return NULL;
}

template <class Node, class Edge>
Edge* GVGraph<Node, Edge>::addEdge(Node* source, Node* target)
{
    if (_nodes.contains(source->name()) && _nodes.contains(target->name()))
    {
        QPair<QString, QString> key(source->name(), target->name());
        if (!_edges.contains(key))
        {
            Edge *e = new Edge(this, source, target);
            _edges.insert(key, e);
            return e;
        }
        else
            return _edges[key];
    }
    return NULL;
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::removeEdge(const QString &source, const QString &target)
{
    removeEdge(QPair<QString, QString>(source, target));
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::removeEdge(Node* source, Node* target)
{
    removeEdge(QPair<QString, QString>(source->name, target->name));
}


template <class Node, class Edge>
void GVGraph<Node, Edge>::removeEdge(Edge* e)
{
    removeEdge(QPair<QString, QString>(e->source()->name, e->target()->name));
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::removeEdge(const QPair<QString, QString>& key)
{
    if (_edges.contains(key))
    {
        Edge *edge = _edges[key];        
        _edges.remove(key);        
        delete edge;
    }
}

template <class Node, class Edge>
QList<Node*> GVGraph<Node, Edge>::nodes() const
{
    QList<Node*> result;
    for (typename QMap<QString, Node*>::const_iterator it = _nodes.begin(); it != _nodes.end(); it++)
        result << *it;

    return result;
}

template <class Node, class Edge>
QList<Edge*> GVGraph<Node, Edge>::edges() const
{
    QList<Edge*> result;
    for (typename QMap<QPair<QString, QString>, Edge*>::const_iterator it = _edges.begin(); it != _edges.end(); it++)
        result << *it;

    return result;
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::setFont(QFont font)
{
    _font = font;

    ag::agset(_graph, "fontname", font.family());
    ag::agset(_graph, "fontsize", QString("%1").arg(font.pointSizeF())); // in points

    ag::agnodeattr(_graph, "fontname", font.family());
    ag::agnodeattr(_graph, "fontsize", QString("%1").arg(font.pointSizeF())); // in points

    ag::agedgeattr(_graph, "fontname", font.family());
    ag::agedgeattr(_graph, "fontsize", QString("%1").arg(font.pointSizeF())); // in points
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::applyLayout(QString engine)
{
    ag::agset(_graph, "overlap", "false");
    ag::agset(_graph, "splines", "true");
    ag::agset(_graph, "pad", QString("%1").arg(points2inches(60.0))); // space around the graph, in inches
    ag::agset(_graph, "dpi", QString("%1").arg(DotDefaultDPI));
    //ag::agset(_graph, "nodesep", QString("%1").arg(1.0)); // space around the node, in inches (in dot, possibly in neato also)
    //gvFreeLayout(_context, _graph);
    ag::gvLayout(_context, _graph, engine);
    //gvRender(_context, _graph, "dot", 0);
    // This is just to test whether the rendering is coherent
    /* FILE* f = fopen("prova0.png", "w");
    gvRender(_context, _graph, "png", f);
    fclose(f); */
    updateNodesLayout(true);
    updateEdgesLayout(true);
    QGraphicsScene::setSceneRect(QRectF(graphviz2viewport(QPointF(ag::data(_graph)->bb.LL.x, ag::data(_graph)->bb.UR.y)),
                            graphviz2viewport(QPointF(ag::data(_graph)->bb.UR.x, ag::data(_graph)->bb.LL.y)))); // in points
    gvFreeLayout(_context, _graph);
    _laid_out = true;
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::updateLayout(bool all)
{
    ag::agset(_graph, "splines", "true");
    ag::agset(_graph, "dpi", QString("%1").arg(DotDefaultDPI));
    for (typename QMap<QString, Node*>::const_iterator it = _nodes.begin(); it != _nodes.end(); it++)
    {
        Node* node = it.value();
        if (node->_laid_out)
        {
            QPointF p = viewport2graphviz(node->pos() + node->boundingRect().center());
            QString pos = QString("%1,%2!").arg(p.x()).arg(p.y()); // in points
            ag::agnodeattr(*node, "pos", pos);
            ag::agnodeattr(*node, "pin", "true");
        }
    }
    //::gvFreeLayout(_context, _graph);
    ag::gvLayout(_context, _graph, "nop2");
    /* FILE* f = fopen("prova.png", "w");
    gvRender(_context, _graph, "png", f);
    fclose(f); */
    updateNodesLayout(all);
    updateEdgesLayout(all);
    QGraphicsScene::setSceneRect(QRectF(graphviz2viewport(QPointF(ag::data(_graph)->bb.LL.x, ag::data(_graph)->bb.UR.y)),
                            graphviz2viewport(QPointF(ag::data(_graph)->bb.UR.x, ag::data(_graph)->bb.LL.y)))); // in points
    gvFreeLayout(_context, _graph);
    _laid_out = true;
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::circleLayout(qreal radius)
{
    ag::agset(_graph, "splines", "true");
    ag::agset(_graph, "pad", "0.2");
    ag::agset(_graph, "dpi", QString("%1").arg(DotDefaultDPI));
    ag::agset(_graph, "nodesep", "0.4");
    //ag::agset(_graph, "page", "400,300");
    //ag::agset(_graph, "ratio", "compress");
    ag::agset(_graph, "size", QString("%1,%1!").arg(points2inches(radius + 30.0))); // in inches
    unsigned int n = _nodes.size();
    //radius = 1.0 / sin(2.0*M_PI/n) + 0.2;
    int i = 0;
    for (typename QMap<QString, Node*>::const_iterator it = _nodes.begin(); it != _nodes.end(); it++)
    {
        Node *node = it.value();
        qreal x = radius * cos(2.0 * M_PI * i / n), y = radius * sin(2.0 * M_PI * i / n);
        QString pos = QString("%1,%2!").arg(x).arg(y);
        ag::agnodeattr(*node, "pos", pos); // in points
        i++;
    }
    ag::gvLayout(_context, _graph, "nop");
    updateNodesLayout(true);
    updateEdgesLayout(true);
    QGraphicsScene::setSceneRect(QRectF(graphviz2viewport(QPointF(ag::data(_graph)->bb.LL.x, ag::data(_graph)->bb.UR.y)),
                            graphviz2viewport(QPointF(ag::data(_graph)->bb.UR.x, ag::data(_graph)->bb.LL.y)))); // in points
    gvFreeLayout(_context, _graph);
    _laid_out = true;
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::updateNodesLayout(bool all_nodes)
{
    for (typename QMap<QString, Node*>::const_iterator it = _nodes.begin(); it != _nodes.end(); it++)
    {
        Node* node = it.value();     
        if (!all_nodes && node->laidOut())
            continue;
        //Transform the width and height from inches to pixels
        qreal height = inches2points(ag::data(*node)->height), width = inches2points(ag::data(*node)->width);       
        QPointF p = graphviz2viewport(QPointF(ag::data(*node)->coord.x - width / 2.0, ag::data(*node)->coord.y + height / 2.0)); // in points
        node->setPos(p - node->boundingRect().topLeft());

        node->setNodeHeight(height);
        node->setNodeWidth(width);        
        node->setLaidOut(true);
    }
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::updateEdgesLayout(bool all_edges, QPointF disp)
{
    for (typename QMap<QPair<QString, QString>, Edge*>::const_iterator it = _edges.begin(); it != _edges.end(); it++)
    {
        Edge *edge = it.value();
        if (!all_edges && edge->_laid_out)
            continue;
        //Calculate the path from the spline (only one spline, as the graph is strict. If it
        //wasn't, we would have to iterate over the first list too)
        //Calculate the path from the spline (only one as the graph is strict)
        if ((ag::data(*edge)->spl->list != 0) && (ag::data(*edge)->spl->list->size % 3 == 1))
        {           
            if (ag::data(*edge)->spl->list->sflag)
            {
                edge->_path.moveTo(graphviz2viewport(QPointF(ag::data(*edge)->spl->list->sp.x, ag::data(*edge)->spl->list->sp.y)) + disp); // in points
                edge->_path.lineTo(graphviz2viewport(QPointF(ag::data(*edge)->spl->list->list[0].x, ag::data(*edge)->spl->list->list[0].y)) + disp); // in points
            }
            else
                edge->_path.moveTo(graphviz2viewport(QPointF(ag::data(*edge)->spl->list->list[0].x, ag::data(*edge)->spl->list->list[0].y)) + disp); // in points

            //Loop over the curve points
            for (int i = 1; i < ag::data(*edge)->spl->list->size; i += 3)
                edge->_path.cubicTo(graphviz2viewport(QPointF(ag::data(*edge)->spl->list->list[i].x, ag::data(*edge)->spl->list->list[i].y)) + disp,
                      graphviz2viewport(QPointF(ag::data(*edge)->spl->list->list[i+1].x, ag::data(*edge)->spl->list->list[i+1].y)) + disp,
                      graphviz2viewport(QPointF(ag::data(*edge)->spl->list->list[i+2].x, ag::data(*edge)->spl->list->list[i+2].y)) + disp); // in points

            //If there is an ending point, draw a line to it
            if (ag::data(*edge)->spl->list->eflag)
                edge->_path.lineTo(graphviz2viewport(QPointF(ag::data(*edge)->spl->list->ep.x, ag::data(*edge)->spl->list->ep.y)) + disp); // in points
        }
        edge->_laid_out = true;
    }
}

template <class Node, class Edge>
void GVGraph<Node, Edge>::render()
{
    for (typename QMap<QString, Node*>::iterator it = _nodes.begin(); it != _nodes.end(); it++)
    {
        Node* node = it.value();
        if (!node->laidOut())
        {
            qDebug() << "Trying to render a non laid-out node";
            continue;
        }
        node->render();    
    }

    for (typename QMap<QPair<QString, QString>, Edge*>::iterator it = _edges.begin(); it != _edges.end(); it++)
    {
        Edge* edge = it.value();
        if (!edge->_laid_out)
        {
            qDebug() << "Trying to render a non laid-out edge";
            continue;
        }
        edge->render();        
    }
}

/// A struct containing the information for a GVGraph's node
class GVNode : public QGraphicsItemGroup
{
public:
    GVNode(AbstractGVGraph *g, const QString& name);
    GVNode(AbstractGVGraph *g, const QString& name, Shape shape);
    ~GVNode();
    inline operator Agnode_t* () { return _node; }
    inline QString name() const { return _name; }
    inline bool laidOut() const { return _laid_out; }
    inline void setLaidOut(bool v) { _laid_out = v; }
    inline double nodeHeight() { return _node_height; }
    inline double nodeWidth() { return _node_width; }
    void setNodeHeight(qreal h);
    void setNodeWidth(qreal w);
protected:
    virtual void render();
    /// The reference graph
    AbstractGVGraph* _graph;
    /// The managed node in the graphviz graph
    Agnode_t* _node;
    /// The unique identifier of the node in the graph
    QString _name;

    /// The position of the center point of the node from the top-left corner
    //QPoint _centerPos;

    /// The size of the node in pixels
    double _node_height, _node_width;
    bool _laid_out;
    Shape _shape;
};

#endif // GVGRAPH_H
