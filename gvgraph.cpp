#include "gvgraph.h"
#include "gvwrapper.h"

const qreal AbstractGVGraph::DotDefaultDPI = 72.0;

AbstractGVGraph::AbstractGVGraph(QString name, Shape shape) :
    _name(name),
    _context(gvContext()),
    _graph(ag::agopen(_name, Agstrictdirected)), // Strict directed graph, see libgraph doc
    _default_shape(shape)
{}

AbstractGVGraph::~AbstractGVGraph()
{
    agclose(_graph);
    gvFreeContext(_context);
}

GVNode::GVNode(AbstractGVGraph *g, const QString &n)
: _graph(g), _name(n), _laid_out(false), _shape(g->defaultShape())
{
    _node = ag::agnode(*g, _name);
    ag::agnodeattr(*this, "label", "");
    g->addItem(this);
}

GVNode::GVNode(AbstractGVGraph *g, const QString &n, Shape shape)
: _graph(g), _name(n), _laid_out(false), _shape(shape)
{
    _node = ag::agnode(*g, _name);
    ag::agnodeattr(*this, "label", "");
    g->addItem(this);
}

GVNode::~GVNode()
{
    ag::agdelete(_graph->gvgraph(), _node);
    _graph->removeItem(this);
}

void GVNode::setNodeWidth(qreal width)
{
    ag::agnodeattr(*this, "width", QString("%1").arg(points2inches(width))); // in inches
    _node_width = width;
}

void GVNode::setNodeHeight(qreal height)
{
    ag::agnodeattr(*this, "height", QString("%1").arg(points2inches(height)));// in inches
    _node_height = height;
}

void GVNode::render()
{
    if (!_laid_out)
    {
        qDebug() << "Trying to render the non laid-out node " << _name;
        return;
    }
    QAbstractGraphicsShapeItem* s_node;
    switch (_shape)
    {
        case Ellipse:
            s_node = new QGraphicsEllipseItem(QRectF(this->pos(), QSizeF(_node_width, _node_height)));
            break;
        case Box:
            s_node = new QGraphicsRectItem(QRectF(this->pos(), QSizeF(_node_width, _node_height)));
            break;
        default: break; // FIXME: an exception should be thrown
    }
    addToGroup(s_node);
    QGraphicsTextItem *label = new QGraphicsTextItem(s_node);
    label->setPlainText(_name);
    label->setPos(this->pos() + QPointF(_node_width / 2.0, _node_height / 2.0) - QPointF(label->boundingRect().x() / 2.0, label->boundingRect().y() / 2.0));
    addToGroup(label);
}

