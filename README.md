GVGraph
=======

A (basic) bridge between graphviz and QT for drawing graphs in QT as laid out by dot.

Currently it is just a tentative bunch of classes inspired by this Blog post:

http://www.mupuf.org/blog/2010/07/08/how_to_use_graphviz_to_draw_graphs_in_a_qt_graphics_scene/

It's currently released AS IS, there is space for much improvement (including rendering the scene within OpenGL).

The aim of this library is to avoid using the too bloated code of Tulip for graph visualization in QT.
